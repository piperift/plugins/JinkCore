// Copyright 2015-2019 Piperift. All Rights Reserved.

using UnrealBuildTool;

public class Chemistry : ModuleRules
{
	public Chemistry(ReadOnlyTargetRules TargetRules) : base(TargetRules)
    {
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			"CoreUObject",
			"Engine",
            "JinkCore"
		});
	}
}
