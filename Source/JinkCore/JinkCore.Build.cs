// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class JinkCore : ModuleRules
{
    public JinkCore(ReadOnlyTargetRules TargetRules) : base(TargetRules)
    {
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "Slate",
            "SlateCore",
            "AIModule",
            "RHI",
            "Factions",
            "AIExtension",
            "Attributes"
		});

        if (TargetRules.bBuildEditor == true)
        {
            PublicDependencyModuleNames.AddRange(new string[] {
                "UnrealEd"
            });
        }
    }
}
