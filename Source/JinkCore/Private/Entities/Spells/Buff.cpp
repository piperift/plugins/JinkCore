// Copyright 2015-2019 Piperift. All Rights Reserved.

#include "Buff.h"
#include "Entity.h"
#include "Item.h"

#define LOCTEXT_NAMESPACE "Buff"

const FAttrCategory UBuff::AttributeCategory { "Buff" };

UBuff::UBuff()
    : Super()
{
    Category = EBuffCategory::BC_Buff;
}

void UBuff::Apply(AEntity* Owner) {
    Entity = Owner;
    OnApply();
}

void UBuff::Unapply()
{
    OnUnapply();
}

bool UBuff::CanPickUpItem(TSubclassOf<UItem>& Type)
{
    for (auto& RestrictedType : RestrictedItemsForPickUp) {
        if(Type.Get() == RestrictedType.Get()) {
            return false;
        }
    }

    return _CanPickUpItem(Type);
}

bool UBuff::_CanPickUpItem_Implementation(TSubclassOf<UItem>& Type)
{
    return true;
}


void UBuff::ApplyBuffModification(UPARAM(ref) FFloatAttr& Attribute, UPARAM(ref) FAttrModifier& Mod)
{
    Attribute.AddModifier(Mod, AttributeCategory);
}

void UBuff::RemoveBuffModification(UPARAM(ref) FFloatAttr& Attribute, UPARAM(ref) FAttrModifier& Mod)
{
    Attribute.RemoveModifier(Mod, AttributeCategory);
}

//////////////////////////////////////////////////////////////////////////

#undef LOCTEXT_NAMESPACE
