// Copyright 2015-2019 Piperift. All Rights Reserved.

#include "DeadVolume.h"

void ADeadVolume::EntityEnter(AEntity * Entity)
{
    Entity->Die();
    ReceiveEntityEnter(Entity);
    OnEntityEnter.Broadcast(Entity);
}
