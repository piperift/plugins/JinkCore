// Copyright 2015-2019 Piperift. All Rights Reserved.

#include "EventHandler.h"
#include "Runtime/Launch/Resources/Version.h"

FEventHandler::FEventHandler(UObject* Outer, int Id)
	: Id(Id)
	, Outer(Outer)
{
	bValid = true;
	bActivated = false;
	bPaused = false;
	Length = 1;
}

bool FEventHandler::Start(float _Length)
{
	return StartInternal(_Length);
}

bool FEventHandler::StartInternal(float _Length)
{
	if (IsRunning() || _Length < 0) {
		return false;
	}

	bActivated = true;
	bPaused = false;

	Timer = FTimer();
	Length = _Length;

	return true;
}


void FEventHandler::Pause()
{
	if (!IsRunning())
		return;

	bPaused = true;
}

void FEventHandler::Resume()
{
	if (!IsPaused()) {
		UE_LOG(LogJinkCore, Warning, TEXT("JinkCore: Tried to Resume an event, but it was not paused."));
		return;
	}

	bPaused = false;
}

void FEventHandler::Restart(float _Length)
{
	if (!OnFinish.IsBound()) {
		UE_LOG(LogJinkCore, Warning, TEXT("JinkCore: Tried to Restart an event that is not bounded."));
		return;
	}

	if (_Length < 0) {
		_Length = Length;
	}

	//Reset the Event
	Reset();

	//Start the event again
	StartInternal(_Length);
}

void FEventHandler::Reset()
{
	//Clear the Timer
	Timer = FTimer();
	bActivated = false;
	bPaused = false;
}

void FEventHandler::Tick(float DeltaTime) {
	if (IsRunning() && !IsPaused()) {
		Timer.Tick(DeltaTime);

		if (Timer.GetCurrentTime() > Length)
		{
			//Check if Event is over
			OnExecute();
		}
	}
}

void FEventHandler::OnExecute()
{
	Reset();

	if (OnFinish.IsBound())
	{
		OnFinish.Execute(Id);
	}
}
