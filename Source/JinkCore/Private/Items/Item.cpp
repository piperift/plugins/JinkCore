// Copyright 2015-2019 Piperift. All Rights Reserved.

#include "Item.h"
#include "Buff.h"
#include "Entity.h"

#define LOCTEXT_NAMESPACE "Item"

UItem::UItem(const FObjectInitializer & ObjectInitializer)
    : Super(ObjectInitializer)
{
    DisplayName = "Item";
    Description = LOCTEXT("Description", "");

#if WITH_EDITORONLY_DATA
    DesignerNotes = LOCTEXT("DesignerNotes", "");
#endif

    bUnique = false;
    bUsable = false;
    bStackable = false;
    Count = 0;

    //Properties
    DamageMod      = FAttrModifier();
    LiveMod        = FAttrModifier();
    MovementMod    = FAttrModifier();
    FireRateMod    = FAttrModifier();
    BulletSpeedMod = FAttrModifier();
}

void UItem::PickUp(AEntity * Owner)
{
    if (IsPickedUp() && !bUnique && bStackable) {
        ++Count;
    }

    if (!IsPickedUp() && Owner) {
        Holder = Owner;

        //Registry Holder Dead event
        Holder->JustDiedDelegate.AddDynamic(this, &UItem::HolderJustDied);

        //Apply Modifications
        Holder->Damage.AddModifier(DamageMod);
        Holder->FireRate.AddModifier(FireRateMod);
        Holder->BulletSpeed.AddModifier(BulletSpeedMod);
        Holder->RunSpeed.AddModifier(MovementMod);
        Holder->WalkSpeed.AddModifier(MovementMod);

        //Update MaxLive Modifications and Live value
        const float OldMaxLive = Holder->MaxLive;
        Holder->MaxLive.AddModifier(LiveMod);
        const float NewMaxLive = Holder->MaxLive;
        const float PositiveHealthIncrement = FMath::Max(0.0f, NewMaxLive - OldMaxLive);
        Holder->Live = FMath::Clamp(Holder->Live + PositiveHealthIncrement, 0.0f, NewMaxLive);

        //Apply Buffs
        for (auto& BuffType : BuffsApplied) {
            if (UBuff* NewBuff = Holder->ApplyBuff(BuffType)) {
                BuffsAppliedObjects.Add(NewBuff);
            }
        }

        ++Count;
        OnPickUp(Holder);
    }
}

void UItem::Drop() {
    if (!Holder)
        return;

    OnDrop();

    //Remove Modifications
    Holder->Damage.RemoveModifier(DamageMod);
    Holder->MaxLive.RemoveModifier(LiveMod);
    Holder->FireRate.RemoveModifier(FireRateMod);
    Holder->BulletSpeed.RemoveModifier(BulletSpeedMod);
    Holder->RunSpeed.RemoveModifier(MovementMod);
    Holder->WalkSpeed.RemoveModifier(MovementMod);

    //Update Live
    Holder->Live = FMath::Clamp(Holder->Live, 0.0f, (float)Holder->MaxLive);

    //Remove Buffs
    for (auto* Buff : BuffsAppliedObjects) {
        Holder->RemoveBuff(Buff);
    }

    Holder = nullptr;
    MarkPendingKill();
}

UWorld* UItem::GetWorld() const
{
    if (HasAllFlags(RF_ClassDefaultObject))
    {
        // If we are a CDO, we must return nullptr instead of calling Outer->GetWorld() to fool UObject::ImplementsGetWorld.
        return nullptr;
    }
    if (Holder)
    {
        return Holder->GetWorld();
    }

    return GetOuter()->GetWorld();
}

void UItem::OnPickUp_Implementation(AEntity * Entity)
{}

void UItem::OnDrop_Implementation()
{}

void UItem::NotifyFired_Implementation(bool& canFire) {
    canFire = true;
}

void UItem::HolderJustDied_Implementation(AController * InstigatedBy, AEntity * Killer)
{
}

//////////////////////////////////////////////////////////////////////////

#undef LOCTEXT_NAMESPACE
