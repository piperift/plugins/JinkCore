// Copyright 2015-2019 Piperift. All Rights Reserved.

#include "JinkGameMode.h"
#include "Kismet/GameplayStatics.h"

#include "Entity.h"


AJinkGameMode::AJinkGameMode()
	: bPaused{ false }
{
    // set default pawn class to our Blueprinted character
    DefaultPawnClass = AEntity::StaticClass();
}

bool AJinkGameMode::Pause()
{
	if (!bPaused)
	{
		bPaused = true;

		EventOnGamePause(true);
		OnGamePause.Broadcast(true);

		return true;
	}
	return false;
}

bool AJinkGameMode::Unpause()
{
	if (bPaused)
	{
		bPaused = false;

		EventOnGamePause(false);
		OnGamePause.Broadcast(false);

		return true;
	}
	return false;
}