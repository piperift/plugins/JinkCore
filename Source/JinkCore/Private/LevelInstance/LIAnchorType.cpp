// Copyright 2015-2019 Piperift. All Rights Reserved.

#include "LIAnchorType.h"
#include "LIAnchorTypeInfo.h"
#include "JCGenerationSettings.h"


bool FLIAnchorType::GetAnchorInfo(FLIAnchorTypeInfo& Info) {
    const UJCGenerationSettings* Settings = GetDefault<UJCGenerationSettings>();

    if (Settings->AnchorTypes.Contains(Name)) {
        Info = Settings->AnchorTypes[Name];
        //If the type is not found, return default type info.
        return true;
    }

    Info = FLIAnchorTypeInfo();
    return false;
}