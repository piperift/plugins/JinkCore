// Copyright 2015-2017 Piperift, All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "Engine/StreamableManager.h"
#include "JinkGameMode.generated.h"

class UGameplayStatics;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGamePause, bool, bPaused);


UCLASS()
class JINKCORE_API AJinkGameMode : public AGameModeBase
{
    GENERATED_BODY()

private:

	UPROPERTY()
	bool bPaused;

public:
    AJinkGameMode();

	UFUNCTION(BlueprintCallable, Category = Pause)
	bool Pause();

	UFUNCTION(BlueprintCallable, Category = Pause)
	bool Unpause();

	UFUNCTION(BlueprintPure, Category = Pause)
	FORCEINLINE bool IsPaused() const { return bPaused; }

	UFUNCTION(BlueprintImplementableEvent, Category = Pause, meta = (DisplayName = "On Game Pause"))
	void EventOnGamePause(bool _bPaused);

    UPROPERTY(BlueprintAssignable, Category = Pause)
    FOnGamePause OnGamePause;
};

