// Copyright 2015-2017 Piperift. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpringArmComponent.h"
#include "ComplexSpringArmComponent.generated.h"


/**
* This component tries to maintain its children at a fixed distance from the parent,
* but will retract the children if there is a collision, and spring back when there is no collision.
*
* Example: Use as a 'camera boom' to keep the follow camera for a player from colliding into the world.
*/

UCLASS(ClassGroup = Camera, meta = (BlueprintSpawnableComponent), hideCategories = (Mobility))
class JINKCORE_API UComplexSpringArmComponent : public USpringArmComponent
{
	GENERATED_UCLASS_BODY()

	/** If true, smooth camera movement towards desired length after collision */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraCollision)
	uint32 bSmoothCollisionRelease : 1;

	/** Smooth camera movement speed towards desired length after collision */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraCollision)
	float CollisionSmoothSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraCollision)
	uint32 bDrawCollisionMarkers : 1;


	/** Temporary variables when using camera collision, to record previous camera collisionposition */
	float PreviousCollisionArmLength;

protected:

	/** Updates the desired arm location, calling BlendLocations to do the actual blending if a trace is done */
	virtual void UpdateDesiredArmLocation(bool bDoTrace, bool bDoLocationLag, bool bDoRotationLag, float DeltaTime) override;


	/**
	* This function allows subclasses to blend the trace hit location with the desired arm location;
	* by default it returns bHitSomething ? TraceHitLocation : DesiredArmLocation
	*/
	//virtual FVector BlendLocations(const FVector& DesiredArmLocation, const FVector& TraceHitLocation, bool bHitSomething, float DeltaTime) override;
};
