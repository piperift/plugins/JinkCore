// Copyright 2015-2019 Piperift. All Rights Reserved.

#pragma once

#include <Object.h>
#include <RenderCore.h>

#include "EventHandler.generated.h"


DECLARE_DYNAMIC_DELEGATE_OneParam(FEventFinishDelegate, int32, Id);

/**
 *
 */
USTRUCT(BlueprintType)
struct JINKCORE_API FEventHandler
{
	GENERATED_USTRUCT_BODY()

public:
	FEventHandler() {
		bValid = false;
		bActivated = false;
		Id = 0;
		Length = 1;
	}

	FEventHandler(UObject* _Outer, int32 _Id);


private:

	UPROPERTY()
	bool bValid;

	UPROPERTY()
	bool bPaused;

	UPROPERTY()
	float Length;

	FTimer Timer;

	UPROPERTY()
	int32 Id;

	UPROPERTY()
	bool bActivated;

	UPROPERTY()
	const UObject* Outer;

	UPROPERTY()
	FEventFinishDelegate OnFinish;

	bool StartInternal(float Length);

public:

	// Start the event timer
	bool Start(float Length);

	// Pause the event timer
	void Pause();

	// Resume the event timer
	void Resume();

	//Reset the event and start it.
	void Restart(float Length);

	//Reset the event and start it.
	void Reset();

	//Called when the event is done.
	void OnExecute();


	void Tick(float DeltaTime);

	/** Helpers */
	bool const  IsValid()   { return bValid; }
	bool const  IsRunning() { return bActivated; }
	bool const  IsPaused()  { return IsRunning() && bPaused; }
	float const GetLength() { return Length; }


	class UWorld* GetWorld() const {
		return  Outer ? Outer->GetWorld() : nullptr;
	}

	FEventFinishDelegate & GetOnFinish() { return OnFinish; }
};
